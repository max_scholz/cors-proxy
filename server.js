// Listen on a specific host via the HOST environment variable
var host = process.env.HOST || '0.0.0.0';
// Listen on a specific port via the PORT environment variable
var port = process.env.PORT || 8080;

var originBlacklist = [];
var originWhitelist = ['https://msa.staffbase.rocks', 'https://staffbasetest-stage.staffbase.rocks', 'https://backend.staffbase.com', 'https://app.staffbase.com', 'https://localhost:*', ''];

// var rateLimit = '0 1 app.staffbase.com backend.staffbase.com';
// var checkRateLimit = require('./lib/rate-limit')(rateLimit);

var cors_proxy = require('./lib/cors-anywhere');
cors_proxy.createServer({
  originBlacklist: originBlacklist,
  originWhitelist: originWhitelist,
  requireHeader: ['origin', 'x-requested-with'],
  // checkRateLimit: checkRateLimit,
  removeHeaders: [
    'cookie',
    'cookie2',
    // Strip Heroku-specific headers
    'x-heroku-queue-wait-time',
    'x-heroku-queue-depth',
    'x-heroku-dynos-in-use',
    'x-request-start',
  ],
  redirectSameOrigin: true,
  httpProxyOptions: {
    xfwd: false, // Do not add X-Forwarded-For, etc. headers, because Heroku already adds it.
  },
}).listen(port, host, function() {
  console.log('Running NodeJS App: Staffbase CORS Anywhere on ' + host + ':' + port);
  console.log('Whitelist ' + JSON.stringify(originWhitelist));
  console.log('Blacklist ' + JSON.stringify(originBlacklist));
});
